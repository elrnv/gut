[package]
name = "gut"
version = "0.8.0"
authors = ["Egor Larionov <egor.larionov@gmail.com>"]
description = "Geometry utilities: storing, manipulating and processing geometries"
edition = "2018"
license = "MIT OR Apache-2.0"
homepage = "https://gitlab.com/elrnv/gut"
repository = "https://gitlab.com/elrnv/gut"
documentation = "https://docs.rs/gut"
keywords = ["geometry", "processing", "algorithms", "io"]

[badges]
gitlab = { repository = "elrnv/gut", branch = "master" }

[dependencies]
chashmap = { version = "2", optional = true }
meshx = "0.1"

[dev-dependencies]
rand = "0.8"
approx = "0.5"
bytemuck = "1.7"
num-traits = "0.2"
criterion = { version = "0.3" }

[features]
default = ["io"]
parallel = ["meshx/rayon", "chashmap"]
io = ["meshx/io"]
unstable = ["parallel"]
bench = ["criterion/real_blackbox", "unstable"]

[[bench]]
name = "storage"
harness = false

[[bench]]
name = "map"
harness = false

[[bench]]
name = "zip"
harness = false

[[bench]]
name = "chunks_iter"
harness = false

[[bench]]
name = "return_vec"
harness = false
