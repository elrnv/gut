# `gut`

Geometry UTilities: a library for storing, manipulating and processing geometry constructs.

[![On crates.io](https://img.shields.io/crates/v/gut.svg)](https://crates.io/crates/gut)
[![On docs.rs](https://docs.rs/gut/badge.svg)](https://docs.rs/gut/)
[![Build status](https://gitlab.com/elrnv/gut/badges/master/pipeline.svg)](https://gitlab.com/elrnv/gut/pipelines)


This library is a work in progress and is not ready for production use.

# Features

 - Closed and open intervals (ClosedInterval, OpenInterval)
 - Axis aligned bounding boxes (BBox).
 - Mesh data structures with statically checked attributes on various topologies (in parentheses):
   - PointCloud (vertex)
   - TriMesh (vertex, face, face-vertex, face-edge)
   - PolyMesh (vertex, face, face-vertex, face-edge)
   - TetMesh (vertex, cell, cell-vertex, cell-face)
   - TetMeshExt (vertex, cell, cell-vertex, cell-face, vertex-cell)
 - Mesh merging and splitting algorithms.
 - Mesh transformations: scale/rotate/translate
 - Basic mesh builders:
   - Torus
   - Solid box (tetmesh)
   - Grid
   - Icosahedron
   - Octahedron
 - Tetrahedron mesh (TetMesh) surface extraction.
 - Centroid/volume/area computations.
 - Mesh I/O via Legacy VTK files (feature gated behind the "io" feature)
 - Normal computation.


# License

This repository is licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or https://www.apache.org/licenses/LICENSE-2.0)
 * MIT License ([LICENSE-MIT](LICENSE-MIT) or https://opensource.org/licenses/MIT)

at your option.
